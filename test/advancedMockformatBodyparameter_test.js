'use strict';

var grunt = require('grunt');
var http = require('http');

/*
  ======== A Handy Little Nodeunit Reference ========
  https://github.com/caolan/nodeunit

  Test methods:
    test.expect(numAssertions)
    test.done()
  Test assertions:
    test.ok(value, [message])
    test.equal(actual, expected, [message])
    test.notEqual(actual, expected, [message])
    test.deepEqual(actual, expected, [message])
    test.notDeepEqual(actual, expected, [message])
    test.strictEqual(actual, expected, [message])
    test.notStrictEqual(actual, expected, [message])
    test.throws(block, [error], [message])
    test.doesNotThrow(block, [error], [message])
    test.ifError(value)
*/

function testAdvancedFormatPost(test, path, body, expectedStatus, expectedMessage, failMessage){
    test.expect(3);
    var req = http.request({
      path: path,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=UTF-8'
      },
      port: 8080
    }, function(response) {
      var data = '';
      response.on('data', function (chunk) {
        data += chunk;
      });
      response.on('end', function () {
        test.equal(response.statusCode, expectedStatus);
        test.equal(response.headers['content-type'], 'application/json;charset=UTF-8');
        var responseBody = JSON.parse(data);
        test.equal(responseBody.message, expectedMessage, failMessage);
        test.done();
      });
    });
    //console.log('sent body: ' + body);
    req.write(body);
    req.end();
}

exports.connect_apimock = {
  setUp: function (done) {
    // setup here if necessary
    done();
  },



    bodyparameter_first_match: function(test){
      testAdvancedFormatPost(test,
        '/api/advanced/bodyparameter',
        '{"foo":"foo"}',
        402,
        'foo',
        'bodyparameter {"foo":"foo"} should return {"message":"foo"}');
    },
    bodyparameter_second_match: function(test){
      testAdvancedFormatPost(test,
        '/api/advanced/bodyparameter?foo=bar',
        '{"foo":"bar"}',
        200,
        'bar',
        'bodyparameter {"foo":"bar"} should return {"message":"bar"}');
    },
    bodyparameter_no_match1: function(test){
      testAdvancedFormatPost(test,
        '/api/advanced/bodyparameter',
        '{"foo":"asdf"}',
        201,
        'foofoofoo',
        'bodyparameter {"foo":"asdf"} should return {"message":"foofoofoo"}');
    },
    bodyparameter_no_match2: function(test){
      testAdvancedFormatPost(test,
        '/api/advanced/bodyparameter',
        '{"bar":"asdf"}',
        201,
        'foofoofoo',
        'bodyparameter {"bar":"asdf"} should return {"message":"foofoofoo"}');
    },
    bodyparameter_no_defaultresponse_no_match: function(test){
      test.expect(3);
      var req = http.request({
        path: '/api/advanced/bodyparameter_no_defaultresponse',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json;charset=UTF-8'
        },
        port: 8080
      }, function(response) {
        var data = '';
        response.on('data', function (chunk) {
          data += chunk;
        });
        response.on('end', function () {
          test.equal(response.statusCode,500);
          test.equal(response.headers['content-type'], 'application/json;charset=UTF-8');
          var expected = '{"error":"No response could be found"}';
          test.equal(data, expected, 'should return an error');
          test.done();
        });
      });
      req.write('{"foo":"asdf"}');
      req.end();
    },
    bodyparameter_no_parameters: function(test){
      testAdvancedFormatPost(test,
        '/api/advanced/bodyparameter_no_parameters',
        '{"foo":"bar"}',
        201,
        'foofoofoo',
        'should return {"message":"foofoofoo"}');
    },


    bodyparameter_complex_body_1: function(test){
      testAdvancedFormatPost(test,
        '/api/advanced/bodyparameter_complex_body',
        '{"user":{"firstname":"Luke","lastname":"Skywalker","address":{"street":"Milkyway","zipcode":"12345"}}, "foo":"foo"}',
        500,
        'Firstname and lastname matches',
        'should match two parameters on second level');
    },
    bodyparameter_complex_body_2: function(test){
      testAdvancedFormatPost(test,
        '/api/advanced/bodyparameter_complex_body',
        '{"user":{"firstname":"Luke","lastname":"Macahan","address":{"street":"Milkyway","zipcode":"12345"}}, "foo":"foo"}',
        501,
        'Firstname matches',
        'should match one parameter on second level');
    },
    bodyparameter_complex_body_3: function(test){
      testAdvancedFormatPost(test,
        '/api/advanced/bodyparameter_complex_body',
        '{"user":{"firstname":"Zeb","lastname":"Macahan","address":{"street":"The wild west","zipcode":"55555"}}, "foo":"foo"}',
        502,
        'zipcode matches',
        'should match one parameter on third level');
    },
    bodyparameter_complex_body_4: function(test){
      testAdvancedFormatPost(test,
        '/api/advanced/bodyparameter_complex_body',
        '{"user":{"firstname":"Zeb","lastname":"Macahan","address":{"street":"The wild west","zipcode":"12345"}}, "foo":"bar"}',
        503,
        'foo matches',
        'should match one parameter on first level');
    },
    bodyparameter_complex_body_5: function(test){
      testAdvancedFormatPost(test,
        '/api/advanced/bodyparameter_complex_body',
        '{"foo":"bar"}',
        503,
        'foo matches',
        'should handle a request that do not contain the mock parameters');
    },


    bodyparameters_one_match: function(test){
      testAdvancedFormatPost(test,
        '/api/advanced/bodyparameters',
        '{"foo":"bar"}',
        201,
        'foofoofoo',
        'bodyparameters {"foo":"bar"} should return {"message":"foofoofoo"}');
    },
    bodyparameters_both_matches: function(test){
      testAdvancedFormatPost(test,
        '/api/advanced/bodyparameters',
        '{"foo":"bar","bar":"foo"}',
        403,
        'foobar',
        'bodyparameters {"foo":"bar","bar":"foo"} should return {"message":"foobar"}');
    },




    requestparameter_bodyparameter_request_match: function(test){
      testAdvancedFormatPost(test,
        '/api/advanced/requestparameter_bodyparameter?foo=bar',
        '{"asdf":"asdf"}',
        201,
        'foofoofoo',
        'should return {"message":"foofoofoo"}');
    },
    requestparameter_bodyparameter_body_match: function(test){
      testAdvancedFormatPost(test,
        '/api/advanced/requestparameter_bodyparameter?asdf=asdf',
        '{"bar":"foo"}',
        201,
        'foofoofoo',
        'should return {"message":"foofoofoo"}');
    },
    requestparameter_bodyparameter_both_match: function(test){
      testAdvancedFormatPost(test,
        '/api/advanced/requestparameter_bodyparameter?foo=bar',
        '{"bar":"foo"}',
        404,
        'foobar',
        'should return {"message":"foobar"}');
    }

};
