'use strict';

var grunt = require('grunt');
var http = require('http');

/*
  ======== A Handy Little Nodeunit Reference ========
  https://github.com/caolan/nodeunit

  Test methods:
    test.expect(numAssertions)
    test.done()
  Test assertions:
    test.ok(value, [message])
    test.equal(actual, expected, [message])
    test.notEqual(actual, expected, [message])
    test.deepEqual(actual, expected, [message])
    test.notDeepEqual(actual, expected, [message])
    test.strictEqual(actual, expected, [message])
    test.notStrictEqual(actual, expected, [message])
    test.throws(block, [error], [message])
    test.doesNotThrow(block, [error], [message])
    test.ifError(value)
*/

function testMatching(test, path, expectedFilepath, failMessage){
    test.expect(2);
    http.request({
      path: path,
      method: 'GET',
      port: 8080
    }, function(response) {
      var data = '';
      response.on('data', function (chunk) {
        data += chunk;
      });
      response.on('end', function () {
        test.equal(response.statusCode, 200);
        var expected = grunt.file.read(expectedFilepath);
        test.equal(data, expected, failMessage);
        test.done();
      });
    }).end();
}

function testNoMatch(test, path, failMessage){
    test.expect(1);
    http.request({
      path: path,
      method: 'GET',
      port: 8080
    }, function(response) {
      var data = '';
      response.on('data', function (chunk) {
        data += chunk;
      });
      response.on('end', function () {
        test.equal(response.statusCode, 404, failMessage);
        test.done();
      });
    }).end();
}

exports.urlmatcing = {
  setUp: function (done) {
    // setup here if necessary
    done();
  },
  correct_url: function(test){
    testMatching(test, 
      '/api/simple/users/',
      'test/api/simple/users.json',
      'A correct url that starts with mock url should match');
  },
  matches_but_not_in_beginning: function(test){
    testNoMatch(test, 
      '/foo/api/simple/users/',
      'Must start with the mock url');
  },
  wrong_url: function(test){
    testNoMatch(test, 
      '/foo/simple/users/',
      'A incorrect url should not match');
  }
  
};
