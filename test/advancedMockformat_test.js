'use strict';

var grunt = require('grunt');
var http = require('http');

/*
  ======== A Handy Little Nodeunit Reference ========
  https://github.com/caolan/nodeunit

  Test methods:
    test.expect(numAssertions)
    test.done()
  Test assertions:
    test.ok(value, [message])
    test.equal(actual, expected, [message])
    test.notEqual(actual, expected, [message])
    test.deepEqual(actual, expected, [message])
    test.notDeepEqual(actual, expected, [message])
    test.strictEqual(actual, expected, [message])
    test.notStrictEqual(actual, expected, [message])
    test.throws(block, [error], [message])
    test.doesNotThrow(block, [error], [message])
    test.ifError(value)
*/

function testAdvancedFormatGet(test, path, expectedStatus, expectedMessage, failMessage){
    test.expect(3);
    var requestOptions = {
      path: path,
      method: 'GET',
      port: 8080
    };
    http.request(requestOptions, function(response) {
      var data = '';
      response.on('data', function (chunk) {
        data += chunk;
      });
      response.on('end', function () {
        test.equal(response.statusCode, expectedStatus);
        test.equal(response.headers['content-type'], 'application/json;charset=UTF-8');
        var responseBody = JSON.parse(data);
        test.equal(responseBody.message, expectedMessage, failMessage);
        test.done();
      });
    }).end();
}

exports.connect_apimock = {
  setUp: function (done) {
    // setup here if necessary
    done();
  },
  empty_file: function(test){
    test.expect(3);
    http.request({
      path: '/api/advanced/emptyfile',
      method: 'GET',
      port: 8080
    }, function(response) {
      var data = '';
      response.on('data', function (chunk) {
        data += chunk;
      });
      response.on('end', function () {
        test.equal(response.statusCode, 200);
        test.equal(response.headers['content-type'], 'application/json;charset=UTF-8');
        var expected = '';
        test.equal(data, expected, 'should return an empty string');
        test.done();
      });
    }).end();
  },
  only_defaultresponse: function(test){
    testAdvancedFormatGet(test,
      '/api/advanced/only_defaultresponse',
      201,
      'foofoofoo',
      'should return {"message":"foofoofoo"}');
  },
  only_defaultresponse_no_body: function(test){
    test.expect(3);
    http.request({
      path: '/api/advanced/only_defaultresponse_no_body',
      method: 'GET',
      port: 8080
    }, function(response) {
      var data = '';
      response.on('data', function (chunk) {
        data += chunk;
      });
      response.on('end', function () {
        test.equal(response.statusCode, 201);
        test.equal(response.headers['content-type'], 'application/json;charset=UTF-8');
        var expected = '';
        test.equal(data, expected, 'should return an empty string');
        test.done();
      });
    }).end();
  },
  only_defaultresponse_no_status: function(test){
    testAdvancedFormatGet(test,
      '/api/advanced/only_defaultresponse_no_status',
      200,
      'foofoofoo',
      'should return default status');
  },



  malformedfile: function(test){
    test.expect(3);
    http.request({
      path: '/api/advanced/malformedfile',
      method: 'GET',
      port: 8080
    }, function(response) {
      var data = '';
      response.on('data', function (chunk) {
        data += chunk;
      });
      response.on('end', function () {
        test.equal(response.statusCode, 500);
        test.equal(response.headers['content-type'], 'application/json;charset=UTF-8');
        var expected = '{"error":"Malformed mockfile. See server log"}';
        test.equal(data, expected, 'should return an error');
        test.done();
      });
    }).end();
  },
  malformed_inputbody: function(test){
    test.expect(3);
    var req = http.request({
      path: '/api/advanced/bodyparameter',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=UTF-8'
      },
      port: 8080
    }, function(response) {
      var data = '';
      response.on('data', function (chunk) {
        data += chunk;
      });
      response.on('end', function () {
        test.equal(response.statusCode,500);
        test.equal(response.headers['content-type'], 'application/json;charset=UTF-8');
        var expected = '{"error":"Malformed input body"}';
        test.equal(data, expected, 'should return an error');
        test.done();
      });
    });
    req.write('{');
    req.end();
  }

};
