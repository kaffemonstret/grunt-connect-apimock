'use strict';

var grunt = require('grunt');
var http = require('http');

/*
  ======== A Handy Little Nodeunit Reference ========
  https://github.com/caolan/nodeunit

  Test methods:
    test.expect(numAssertions)
    test.done()
  Test assertions:
    test.ok(value, [message])
    test.equal(actual, expected, [message])
    test.notEqual(actual, expected, [message])
    test.deepEqual(actual, expected, [message])
    test.notDeepEqual(actual, expected, [message])
    test.strictEqual(actual, expected, [message])
    test.notStrictEqual(actual, expected, [message])
    test.throws(block, [error], [message])
    test.doesNotThrow(block, [error], [message])
    test.ifError(value)
*/

function testAdvancedFormatGet(test, path, headers, expectedStatus, expectedMessage, failMessage){
    test.expect(3);
    var requestOptions = {
      path: path,
      method: 'GET',
      port: 8080,
      headers: headers
    };
    http.request(requestOptions, function(response) {
      var data = '';
      response.on('data', function (chunk) {
        data += chunk;
      });
      response.on('end', function () {
        test.equal(response.statusCode, expectedStatus);
        test.equal(response.headers['content-type'], 'application/json;charset=UTF-8');
        var responseBody = JSON.parse(data);
        test.equal(responseBody.message, expectedMessage, failMessage);
        test.done();
      });
    }).end();
}

exports.connect_apimock = {
  setUp: function (done) {
    // setup here if necessary
    done();
  },

  requestheaders_one_of_two_matches: function(test) {
    testAdvancedFormatGet(test,
      '/api/advanced/requestheaders',
      {
        header1: 'one',
        header2: '2'
      },
      402,
      'one',
      'should return {"message": "one"}');
  },
  requestheaders_both_matches: function(test) {
    testAdvancedFormatGet(test,
      '/api/advanced/requestheaders',
      {
        header1: 'one',
        header2: 'two'
      },
      401,
      'onetwo',
      'should return {"message": "onetwo"}');
  },
  requestheaders_two_of_three_matches: function(test) {
    testAdvancedFormatGet(test,
      '/api/advanced/requestheaders',
      {
        header1: 'one',
        header2: 'two',
        header3: 'three'
      },
      401,
      'onetwo',
      'should return {"message": "onetwo"}');
  },
  requestheaders_no_matches: function(test) {
    testAdvancedFormatGet(test,
      '/api/advanced/requestheaders',
      {
        header1: 'foo',
        header2: 'bar'
      },
      201,
      'foofoofoo',
      'should return {"message": "foofoofoo"}');
  }

};
