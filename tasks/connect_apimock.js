/*
 * grunt-connect-apimock
 * git-ljo
 *
 * Copyright (c) 2015 Lars Johansson
 * Licensed under the MIT license.
 */

'use strict';

var util = require('util');
var apimock = require('../lib/apimock');

module.exports = function (grunt) {

  // Please see the Grunt documentation for more information regarding task
  // creation: http://gruntjs.com/creating-tasks

  grunt.registerTask('configureApimock', 'Configure apimock.', function(target){
    var configLocation = 'connect.apimock';
    if(target){
      configLocation = 'connect.'+target+'.apimock';
    }

    var apimockOptions = grunt.config(configLocation);
    if(apimockOptions === undefined){
      grunt.log.error(configLocation + ' configuration is missing!');
      return;
    }

    var hasError = false;
    var optionsArray = util.isArray(apimockOptions) ? apimockOptions : [apimockOptions];
    optionsArray.forEach(function(option, index){
      if(option.url === undefined){
        grunt.log.error('url configuration is missing for option ' + index);
        hasError = true;
      }
      if(option.dir === undefined){
        grunt.log.error('dir configuration is missing for option ' + index);
        hasError = true;
      }
    });

    if(hasError){
      return;
    }

 
    apimock.config(apimockOptions);
  });

};
